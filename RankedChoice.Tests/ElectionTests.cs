using System;
using System.Collections.Generic;
using Xunit;

namespace RankedChoice.Tests
{
    public class ElectionTests
    {
        [Fact]
        public void Election_WithNoVoters_ThrowsException()
        {
            var voters = new List<List<int>>();

            Assert.Throws<ArgumentException>(() => Election.RunElection(voters));
        }
        
        [Fact]
        public void Election_VotersWithNoVotes_ThrowsException()
        {
            var voters = new List<List<int>> 
            {
                new List<int>(),
                new List<int>(),
                new List<int>(),
            };
            
            Assert.Throws<ArgumentException>(() => Election.RunElection(voters));
        }
        
        [Theory]
        [InlineData(new[] {1,2,3}, new[] {1,3,2}, new[] {3,2,1}, 1)]
        [InlineData(new[] {1,2,3}, new[] {2,1,3}, new[] {3,1,2}, 1)]
        public void Election_NormalVotesNoTies_ReturnsWinner(int[] voterA, int[] voterB, int[] voterC, int expectedWinner)
        {
            var voters = new List<List<int>> 
            {
                new List<int>(voterA),
                new List<int>(voterB),
                new List<int>(voterC),
            };

            var winner = Election.RunElection(voters);
            
            Assert.Equal(expectedWinner, winner);
        }
        
        [Fact]
        public void Election_TieVotes_ThrowsException()
        {
            var voters = new List<List<int>> 
            {
                new List<int> { 1, 2, },
                new List<int> { 1, 2, },
                new List<int> { 2, 1, },
                new List<int> { 2, 1, },
            };

            Assert.Throws<TieVote>(() => Election.RunElection(voters));
        }
    }
}