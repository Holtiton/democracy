﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RankedChoice
{
    public static class Election
    {
        public static T RunElection<T>(IEnumerable<IEnumerable<T>> voters)
        {
            var roundVotes = new Dictionary<T, int>();
            var eliminated = new List<T>();

            while (true) {
                roundVotes.Clear();
                var numberOfVoters = 0;

                foreach (var voter in voters)
                {
                    numberOfVoters += 1;
                    var numberOfVotes = 0;
                    foreach (var vote in voter)
                    {
                        numberOfVotes += 1;
                        if (eliminated.Contains(vote)) continue;
                        roundVotes.TryGetValue(vote, out var voteCount);
                        roundVotes[vote] = voteCount + 1;
                        // only count the first non-eliminated vote
                        break;
                    }
                    if (numberOfVotes == 0)
                    {
                        throw new ArgumentException("Voter without votes");
                    }
                }

                if (numberOfVoters == 0)
                {
                    throw new ArgumentException("No voters in election");
                }

                var fiftyPercentVoters = (numberOfVoters + 1) / 2;
                var winners = new List<T>();
                foreach (var (key, voteCount) in roundVotes)
                {
                    if (voteCount >= fiftyPercentVoters) {
                        winners.Add(key);
                    }
                }

                switch (winners.Count)
                {
                    case 1:
                        return winners[0];
                    case 2:
                        throw new TieVote();
                    default:
                        eliminated.Add(roundVotes.Aggregate((l, r) => l.Value < r.Value ? l : r).Key);
                        break;
                }
            }
        }
    }
    public class TieVote : Exception {
        public TieVote()
        {
        }

        public TieVote(string message)
            : base(message)
        {
        }

        public TieVote(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}