using System.Linq;
using System.Security.Claims;
using System.Threading;
using Democracy.Controllers;
using Democracy.Data;
using Democracy.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Democracy.Tests
{
    public class VoteControllerTests
    {
        private AppDbContext _context;
        private UserManager<IdentityUser> _userManager;

        public VoteControllerTests()
        {
            InitDbContext();
            InitIdentityContext();
        }


        [Fact(Skip = "Dont know how to fix identity yet")]
        public void SubmitVotes_NewVoter_VoteIsAdded()
        {
            var controller = new VoteController(_context, _userManager)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = new ClaimsPrincipal(
                            new ClaimsIdentity(
                                new[] {new Claim(ClaimTypes.Name, "Test2")}, "someAuthTypeName"))
                    }
                }
            };


            controller.SubmitVotes(new Votes() { votes = new[] {1,2,3} });
            
            Assert.Equal(5,_context.Votes.Count());
        }
        
        [Fact(Skip = "Dont know how to fix identity yet")]
        public void SubmitVotes_ExistingVoter_VotesCleared()
        {
            var controller = new VoteController(_context, _userManager)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = new ClaimsPrincipal(
                            new ClaimsIdentity(
                                new[] {new Claim(ClaimTypes.NameIdentifier, "Test")}, "someAuthTypeName"))
                    },
                }
            };

            controller.SubmitVotes(new Votes() { votes = new[] {1,2,3} });

            Assert.Equal(3,_context.Votes.Count());
        }

        private void InitDbContext()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var builder = new DbContextOptionsBuilder<AppDbContext>().UseSqlite(connection);

            var context = new AppDbContext(builder);
            context.Database.EnsureCreated();

            var vote = new Vote
            {
                UserId = "Test",
                ItemId = 1,
                Ordinal = 1,
            };
            var vote2 = new Vote
            {
                UserId = "Test",
                ItemId = 2,
                Ordinal = 2,
            };
            context.Votes.Add(vote);
            context.Votes.Add(vote2);
            context.SaveChanges();
            _context= context;
        }

        private void InitIdentityContext()
        {
            var mockUserStore = new Mock<IUserStore<IdentityUser>>();
            mockUserStore.Setup(x => x.FindByIdAsync("Test", CancellationToken.None))
                .ReturnsAsync(new IdentityUser()
                {
                    Id = "Test",
                });
            
            var userManager = new UserManager<IdentityUser>(mockUserStore.Object,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
            _userManager = userManager;
        }
    }
}
