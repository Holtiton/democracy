using System.Collections.Generic;
using System.Linq;
using Democracy.Controllers;
using Democracy.Data;
using Democracy.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Democracy.Tests
{
    public class HomeControllerTests
    {
        [Fact]
        public void Index_WithoutVotes_ModelIsNull()
        {
            var context = CreateDbContext(Enumerable.Range(1, 10).Select(i => new Item
            {
                Title = $"Test title {i}",
                Description = $"Test description {i}",
                Done = false,
                Approved = true
            }), null);

            var controller = new HomeController(context);

            var model = controller.Index()?.ViewData.Model as Item;

            Assert.Null(model);
        }

        [Fact]
        public void Index_WithVotes_ModelHasCurrentWinner()
        {
            var items = Enumerable.Range(1, 10).Select(i => new Item
            {
                Title = $"Test title {i}",
                Description = $"Test description {i}",
                Done = false,
                Approved = true,
            });

            var votes = new List<Vote>
            {
                new Vote
                {
                    UserId = $"User",
                    ItemId = 2,
                    Ordinal = 1,
                },
                new Vote
                {
                    UserId = $"User",
                    ItemId = 3,
                    Ordinal = 2,
                }
            };

            var context = CreateDbContext(items, votes);

            var controller = new HomeController(context);

            var model = controller.Index()?.ViewData.Model as Item;

            Assert.Equal(2, model?.ItemId);
        }

        [Fact]
        public void Index_WinnerItemIsDone_ModelHasNextWinner()
        {
            var items = Enumerable.Range(1, 10).Select(i => new Item
            {
                Title = $"Test title {i}",
                Description = $"Test description {i}",
                Done = false,
                Approved = true,
            }).ToList();

            var votes = new List<Vote>
            {
                new Vote
                {
                    UserId = $"User",
                    ItemId = 2,
                    Ordinal = 1,
                },
                new Vote
                {
                    UserId = $"User",
                    ItemId = 3,
                    Ordinal = 2,
                }
            };

            items[1].Done = true;

            var context = CreateDbContext(items, votes);

            var controller = new HomeController(context);

            var model = controller.Index()?.ViewData.Model as Item;

            Assert.Equal(3, model?.ItemId);
        }

        [Fact]
        public void Index_WithTieVotes_ModelIsNull()
        {
            var items = Enumerable.Range(1, 10).Select(i => new Item
            {
                Title = $"Test title {i}",
                Description = $"Test description {i}",
                Done = false,
                Approved = true,
            });

            var votes = new List<Vote>
            {
                new Vote
                {
                    UserId = $"User1",
                    ItemId = 1,
                    Ordinal = 1,
                },
                new Vote
                {
                    UserId = $"User",
                    ItemId = 2,
                    Ordinal = 1,
                }
            };

            var context = CreateDbContext(items, votes);

            var controller = new HomeController(context);

            var model = controller.Index()?.ViewData.Model as Item;

            Assert.Null(model);
        }

        private AppDbContext CreateDbContext(IEnumerable<Item> items, IEnumerable<Vote> votes)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var builder = new DbContextOptionsBuilder<AppDbContext>().UseSqlite(connection);

            var context = new AppDbContext(builder);
            context.Database.EnsureCreated();

            if (items != null)
            {
                context.Items.AddRange(items);
            }

            if (votes != null)
            {
                context.Votes.AddRange(votes);
            }

            context.SaveChanges();
            return context;
        }
    }
}