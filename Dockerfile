FROM microsoft/dotnet:2.2-sdk AS build-env
WORKDIR /app

COPY *.sln .
COPY Democracy/*.csproj ./Democracy/
COPY Democracy.Tests/*.csproj ./Democracy.Tests/
COPY RankedChoice/*.csproj ./RankedChoice/
COPY RankedChoice.Tests/*.csproj ./RankedChoice.Tests/
RUN dotnet restore

# Copy everything else and build
COPY Democracy/. ./Democracy/
COPY Democracy.Tests/. ./Democracy.Tests/
COPY RankedChoice/. ./RankedChoice/
COPY RankedChoice.Tests/. ./RankedChoice.Tests/
RUN dotnet test
RUN dotnet publish -c DockerRelease -o out

# Build runtime image
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /app
ENV ASPNETCORE_ENVIRONMENT=Production
COPY --from=build-env /app/Democracy/out .
ENTRYPOINT ["dotnet", "Democracy.dll"]
