# Democracy

Website for voting (based on ranked choice) on whatever.

## Features

- Voting
- Election (runs when accessing homepage)
- Suggest new item for voting
- Register / Login / Logout
- Admin views

## Administration

There is a default admin account. The one to start the service should register himself and promote himself to admin and delete the default admin.

Admins can accept suggestion and mark suggestions as completed (which essentially deletes them). Admins also can promote other users to admins and demote admins to users.

**Note:** You can probably break the app by demoting the only admin. Haven't tried.

## Setup

1. Clone repository
2. Restore
    - `dotnet restore`
3. Run
    - `dotnet run --project Democracy` (if in solution level folder)

Alternatively with Docker instead of step 3:

3. Build image
    - `docker build -t democracy .`

4. Run in Docker container
    - `docker run -d -p 8080:80 --name democracy_app democracy` or
    - `docker run -it --rm -p 8080:80 --name democracy_app democracy` 

**Note:** Remember to clear cookies if developing locally and trying to run in Docker later.

## Migration

If model changes are required nuke the Migrations folder and create them again with:
- `dotnet ef migrations add <nameOfMigration> --context AppDbContext`
- `dotnet ef migrations add <nameOfMigrationForIdentity> --context AppIdentityDbContext`

This would be unnecessary if I knew how EF Core works but it doesn't matter here.

## Databases

Uses SQLite for votes and items. Users are stored in a separate DB (SQLite also) managed by Identity Framework.

If any changes are required its almost certainly easier to nuke the DBs (eg. when the model changes). They are stored in data.db and identity.db

Migrations should run on startup which create the DBs so no need to create them separately.

## Todo

- [ ] Features
    - [ ] Password reset
    - [ ] Show registered votes on the votes page instead of list sorted by ItemId
- [ ] Quality
    - [ ] Consistent styling over all Razor pages
    - [ ] Consistent return values from Controllers (IActionResult vs ViewResult etc)
    - [ ] Proper way to handle DataProtection (Docker conditional compile in startup.cs)
- [ ] Tests
    - [x] Ranked choice (admittedly poorly)
    - [x] HomeController
    - [ ] VoteController
    - [ ] ItemController
    - [ ] AccountController
    - [ ] SuggestionController
    - [ ] AdminController

## Dependencies (in tree)

- Bootstrap 4
- Sortable.js

# Ranked Choice

Implementation of ranked choice algorithm. Accepts `IEnumerable<IEnumerable<T>>` for voting.
