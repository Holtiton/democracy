using Democracy.Data;
using Democracy.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Democracy.Controllers 
{
    [Authorize]
    public class VoteController : Controller
    {
        private AppDbContext _context;
        private UserManager<IdentityUser> _userManager;

        public VoteController(AppDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpPost]
        public async void SubmitVotes([FromBody] Votes newVotes)
        {
            if (!ModelState.IsValid) return;

            var user = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            var userVotes = _context.Votes.Where(v => v.UserId == user);
            _context.Votes.RemoveRange(userVotes);

            for (var i = 0; i < newVotes.votes.Length; i++)
            {
                var newVote = new Vote {
                    UserId = user,
                    ItemId = newVotes.votes[i],
                    Ordinal = i
                };
                _context.Votes.Add(newVote);
            }
            _context.SaveChanges();
        }
    }
}
