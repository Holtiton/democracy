using System;
using System.Collections.Generic;
using System.Linq;
using Democracy.Data;
using Microsoft.AspNetCore.Mvc;
using RankedChoice;

namespace Democracy.Controllers
{
    public class HomeController : Controller
    {
	private AppDbContext _context;

        public HomeController(AppDbContext context)
        {
	    _context = context;
        }

        public ViewResult Index()
        {
            if (!_context.Votes.Any()) return View();

            var votesPerUser = new List<List<int>>();
            // TODO: Some optimization can be done here as EF core is complaining
            foreach (var userVotes in _context.Votes.GroupBy(v => v.UserId))
            {
                votesPerUser.Add(userVotes.OrderBy(v => v.Ordinal).Where(v => {
                    var item = _context.Items.FirstOrDefault(i => i.ItemId == v.ItemId);
                    if (item != null && !item.Done) { return true; } else { return false; };
                }).Select(v => v.ItemId).ToList());
            }

            try
            {
                var electionWinnerId = Election.RunElection(votesPerUser);
                var electionWinner = _context.Items.FirstOrDefault(i => i.ItemId == electionWinnerId);
                return electionWinner != null ? View(electionWinner) : View();
            }
            catch (ArgumentException)
            {
                return View();
            }
            catch (TieVote)
            {
                return View();
            }
        }
    }
}
