using System.Threading.Tasks;
using Democracy.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Democracy.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private UserManager<IdentityUser> _userManager;
        private SignInManager<IdentityUser> _signInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginDetails details, string returnUrl)
        {
            if (!ModelState.IsValid) return View(details);

            var user = await _userManager.FindByEmailAsync(details.Email);
            if (user == null) return View(details);

            await _signInManager.SignOutAsync();
            var result = await _signInManager.PasswordSignInAsync(user, details.Password, false, false);
            if (result.Succeeded)
            {
                return Redirect(returnUrl ?? "/");
            }

            ModelState.AddModelError(nameof(LoginDetails.Email),"Invalid login");
            return View(details);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ViewResult Register(string returnUrl)
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterDetails details, string returnUrl)
        {
            if (!ModelState.IsValid) return View(details);

            var entry = await _userManager.FindByEmailAsync(details.Email);
            if (entry != null) return View(details);

            var newUser = new IdentityUser
            {
                UserName = details.Name,
                Email = details.Email,
            };

            var result = await _userManager.CreateAsync(newUser, details.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(newUser, "User");
                return await Login(new LoginDetails { Email = details.Email, Password = details.Password }, "/");
            }
            ModelState.AddModelError(nameof(RegisterDetails.Email), "Account creation failed.");

            return View(details);
        }
    }
}
