using System.Linq;
using System.Threading.Tasks;
using Democracy.Data;
using Democracy.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Democracy.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private AppDbContext _context;
        private UserManager<IdentityUser> _userManager;

        public AdminController(AppDbContext context, UserManager<IdentityUser> userManager)
        {
	    _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> List()
        {
            var stuff = new AdminList
            {
                Items = _context.Items.Where(item => !item.Done),
                Votes = _context.Votes,
                Users = await _userManager.GetUsersInRoleAsync("User"),
                Admins = await _userManager.GetUsersInRoleAsync("Administrator")
            };
            return View(stuff);
        }

        [HttpPost]
        public IActionResult Approve(int id)
        {
            var itemForApproval = _context.Items.FirstOrDefault(i => i.ItemId == id);
            if (itemForApproval == null) return RedirectToAction("List");

            itemForApproval.Approved = true;
	    _context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpPost]
        public IActionResult Complete(int id)
        {
            var itemForCompletion = _context.Items.FirstOrDefault(i => i.ItemId == id);
            if (itemForCompletion == null) return RedirectToAction("List");

            itemForCompletion.Done = true;
	    _context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpPost]
        public async Task<IActionResult> Promote(string email)
        {
            var entry = await _userManager.FindByEmailAsync(email);
            if (entry == null) return RedirectToAction("List");

            await _userManager.RemoveFromRoleAsync(entry, "User");
            await _userManager.AddToRoleAsync(entry, "Administrator");

            return RedirectToAction("List");
        }

        [HttpPost]
        public async Task<IActionResult> Demote(string email)
        {
            var entry = await _userManager.FindByEmailAsync(email);
            if (entry == null) return RedirectToAction("List");

            await _userManager.RemoveFromRoleAsync(entry, "Administrator");
            await _userManager.AddToRoleAsync(entry, "User");

            return RedirectToAction("List");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string email)
        {
            var entry = await _userManager.FindByEmailAsync(email);
            if (entry != null)
            {
                await _userManager.DeleteAsync(entry);
            }

            return RedirectToAction("List");
        }
    }
}
