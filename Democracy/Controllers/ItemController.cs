using System.Linq;
using Democracy.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Democracy.Controllers
{
    [Authorize]
    public class ItemController : Controller
    {
        private AppDbContext _context;

        public ItemController(AppDbContext context)
        {
            _context = context;
        }

        public ViewResult List() => View(_context.Items.Where(item => item.Approved && !item.Done));
    }
}
