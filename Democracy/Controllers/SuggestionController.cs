using Democracy.Data;
using Democracy.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Democracy.Controllers
{
    [Authorize]
    public class SuggestionController : Controller
    {
        private AppDbContext _context;

        public SuggestionController(AppDbContext context)
        {
            _context = context;
        }

        public IActionResult New(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public IActionResult New(NewSuggestion suggestion, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var newItem = new Item 
                { 
                    Title = suggestion.Title, 
                    Description = suggestion.Description, 
                    Approved = false,
                    Done = false,
                };
		_context.Add(newItem);
		_context.SaveChanges();
                return Redirect(returnUrl ?? "/");
            }
            ViewBag.returnUrl = returnUrl;
            return View();
        }
    }
}
