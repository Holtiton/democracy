using System.Linq;
using Democracy.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Democracy.Data 
{
    public static class SeedAppData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetRequiredService<AppDbContext>();
            context.Database.Migrate();
            
            if (!context.Items.Any()) 
            {
                context.Items.AddRange(
                    new Item
                    {
                        ItemId = 1,
                        Title = "Some test item 1",
                        Description = "Some test item description 1",
                        Done = false,
                        Approved = true,
                    },
                    new Item
                    {
                        ItemId = 2,
                        Title = "Some test item 2",
                        Description = "Some test item description 2",
                        Done = false,
                        Approved = true,
                    },
                    new Item
                    {
                        ItemId = 3,
                        Title = "Some test item 3",
                        Description = "Some test item description 3",
                        Done = false,
                        Approved = true,
                    },
                    new Item
                    {
                        ItemId = 4, 
                        Title = "Some test item 4", 
                        Description = "Some test item description 4",
                        Done = false,
                        Approved = false
                    }
                );
            }

            context.SaveChanges();
        }
    }
}