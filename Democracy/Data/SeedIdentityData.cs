using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Democracy.Data
{
    public static class SeedIdentityData
    {
        public static async void EnsurePopulated(IApplicationBuilder app, IConfiguration configuration)
        {
	        var adminUser = configuration["Data:AdminUser:Name"];
	        var adminEmail = configuration["Data:AdminUser:Email"];
	        var adminPassword = configuration["Data:AdminUser:Password"];

            var context = app.ApplicationServices.GetRequiredService<AppIdentityDbContext>();
            context.Database.Migrate();

            var userManager = app.ApplicationServices.GetRequiredService<UserManager<IdentityUser>>();
            var roleManager = app.ApplicationServices.GetRequiredService<RoleManager<IdentityRole>>();
            
            var adminRole = await roleManager.FindByNameAsync("Administrator");
            if (adminRole == null)
            {
                await roleManager.CreateAsync(new IdentityRole("Administrator"));
            }

            var userRole = await roleManager.FindByNameAsync("User");
            if (userRole == null)
            {
                await roleManager.CreateAsync(new IdentityRole("User"));
            }

            var user = await userManager.FindByEmailAsync(adminEmail);
            if (user == null)
            {
                user = new IdentityUser { UserName = adminUser, Email = adminEmail };
                await userManager.CreateAsync(user, adminPassword);
                await userManager.AddToRoleAsync(user, "Administrator");
            }
        }
    }
}
