using Democracy.Models;
using Microsoft.EntityFrameworkCore;

namespace Democracy.Data {
    public class AppDbContext : DbContext {

        public AppDbContext(DbContextOptionsBuilder<AppDbContext> builder) : base(builder.Options) 
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Vote> Votes { get; set; }
    }
}