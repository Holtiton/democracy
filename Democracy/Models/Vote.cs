namespace Democracy.Models
{
    public class Vote
    {
        public int VoteId { get; set; }
        public string UserId { get; set; }
        public int ItemId { get; set; }
        public int Ordinal { get; set; }
    }
}