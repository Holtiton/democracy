using System.ComponentModel.DataAnnotations;
// ReSharper disable Mvc.TemplateNotResolved

namespace Democracy.Models
{
    public class RegisterDetails
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [UIHint("email")]
        public string Email { get; set; }

        [Required]
        [UIHint("password")]
        public string Password { get; set; }
    }
    public class LoginDetails
    {
        [Required]
        [UIHint("email")]
        public string Email { get; set; }

        [Required]
        [UIHint("password")]
        public string Password { get; set; }
    }
}