using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Democracy.Models
{
    public class AdminList
    {
        public IEnumerable<Vote> Votes { get; set; }
        public IEnumerable<Item> Items { get; set; }
        public IEnumerable<IdentityUser> Users { get; set; }
        public IEnumerable<IdentityUser> Admins { get; set; }
    }
}