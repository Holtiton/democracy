using System.ComponentModel.DataAnnotations;

namespace Democracy.Models
{
    public class NewSuggestion
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }
    }
}